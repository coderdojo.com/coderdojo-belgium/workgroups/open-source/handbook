---
title: The CoderDojo Belgium Handbook
---

{{< blocks/cover title="Welcome to the CoderDojo Belgium Handbook" image_anchor="top" color="primary" >}}

<form class="td-sidebar__search d-flex align-items-center" style="margin: 1.5em 0em;">
    <div class="td-search td-search--offline">
  <div class="td-search__icon"></div>
  <input type="search" class="bg-white text-black td-search__input form-control"  style="padding-left: 2em;" placeholder="Search the Handbook…" aria-label="Search the Handbook…" autocomplete="off" data-offline-search-index-json-src="/offline-search-index.json" data-offline-search-base-href="/" data-offline-search-max-results="10">
</div>
<button class="btn btn-link td-sidebar__toggle d-md-none p-0 ms-3 fas fa-bars" type="button" data-bs-toggle="collapse" data-bs-target="#td-section-nav" aria-controls="td-section-nav" aria-expanded="false" aria-label="Toggle section navigation"></button>
  </form>

{{% /blocks/cover %}}

{{< homepage-data-toc >}}
