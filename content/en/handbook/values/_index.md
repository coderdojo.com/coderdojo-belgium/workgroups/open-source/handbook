---
title: CoderDojo Belgium Values
description: Learn more about how we live our values at CDJ BE
canonical_path: "/handbook/values/"
images:
    - /images/opengraph/CoderDojo_values_handbook_social_card.png
no_list: true
cascade:
      type: docs
---

## CREDIT

<!-- markdownlint-disable MD051 -->
CoderDojo's six core values are
[**🤝 Collaboration**](#collaboration),
[**📈 Results**](#results),
[**⏱️ Efficiency**](#efficiency),
[**🌐 Diversity, Inclusion & Belonging**](#diversity-inclusion),
[**👣 Iteration**](#iteration), and
[**👁️ Transparency**](#transparency),
and together they spell the **CREDIT** we give each other by assuming
good intent. We react to them [with values
emoji](/handbook/communication/#values-emoji) and they are made
actionable below.

## 🤝 Collaboration{#collaboration}

To achieve results, team members must work together effectively. At CoderDojo, helping others is a priority, even when it is not immediately related to the goals that you are trying to achieve.
Similarly, you can rely on others for help and advice—in fact, you're expected to do so.
Anyone can chime in on any subject, including people who don't work at CoderDojo.
The person who's responsible for the work decides how to do it,
but they should always take each suggestion seriously and try to respond and explain why it may or may not have been implemented.

##### Kindness

We value caring for others.
Demonstrating we care for people provides an effective framework for challenging directly and delivering feedback.
We disagree with organisations that say [Evaluate People Accurately, Not "Kindly"](https://inside.bwater.com/publications/principles_excerpt).
We're all for accurate assessment, but we think it must be done in a kind way.
Give as much positive feedback as you can, and do it in a public way.

##### Share

There are aspects of CoderDojo culture, such as intentional transparency, that are unintuitive to outsiders and new team members.
Be willing to invest in people and engage in open dialogue.
For example, consider making private issues public wherever possible so that we can all learn from the experience. Don't be afraid of judgement or scrutiny when sharing publicly, we all understand [it's impossible to know everything](#its-impossible-to-know-everything).

Everyone can **remind** anyone in the company about our values.
If there is a disagreement about the interpretations, the discussion can be escalated to more people within the company without repercussions.

Share problems you run into, ask for help, be forthcoming with information and **speak up**.

##### Negative feedback is 1-1

Give negative feedback in the smallest setting possible.
One-on-one video calls are preferred.

Negative *feedback* is distinct from negativity and disagreement. If there is no direct feedback involved, strive to discuss disagreement [in a public channel](/handbook/communication/#use-public-channels), respectfully and [transparently](#transparency).

##### Provide feedback in a timely manner

We want to solve problems while they are **small**.

##### Give feedback effectively

Giving feedback is challenging, but it's important to deliver it effectively.
When providing feedback, always make it about the work itself;
focus on the business impact and not the person.
Make sure to provide at least one clear and recent example.
If a person is going through a hard time in their personal life, then take that into account.

##### Assume positive intent

We naturally have a double standard when it comes to the actions of others.
We blame circumstances for our own mistakes, but individuals for theirs.
This double standard is called the [Fundamental Attribution Error](https://en.wikipedia.org/wiki/Fundamental_attribution_error).
In order to mitigate this bias, you should always [assume positive intent](https://www.collaborativeway.com/general/a-ceos-advice-assume-positive-intent/) in your interactions with others, respecting their expertise and giving them grace in the face of what you might perceive as mistakes.

When [disagreeing](#disagree-commit-and-disagree), folks sometimes argue against the weakest points of an argument, or an imaginary argument (e.g. ["straw man"](https://en.wikipedia.org/wiki/Straw_man)). Assume the points are presented in good faith, and instead try to argue against the strongest version of your opponent’s position. We call this arguing against a "steel" position, instead of a "straw" one. This concept is borrowed from [argue the "steel man"](https://constantrenewal.com/steel-man) technique.

A "steel" position should be against the absolute most effective version of your opponent’s position — potentially even more compelling than the one they presented. A good "steel" position is one where the other person feels you've represented their position well, even if they still disagree with your assumptions or conclusion.

##### Address behavior, but don't label people

There is a lot of good in [this article](http://bobsutton.typepad.com/my_weblog/2006/10/the_no_asshole_.html) about not
wanting jerks on our team, but we believe that **jerk** is a label for behavior rather than an inherent classification
of a person.  We avoid classifications.

##### Say sorry

If you made a mistake, apologize as soon as possible.
Saying sorry is not a sign of weakness but one of strength.
The people that do the most work will likely make the most mistakes.
Additionally, when we share our mistakes and bring attention to them, others can learn from us, and the same mistake is less likely to be repeated by someone else.
Mistakes can include when you have not been kind to someone. In order to reinforce our values, it is important, and takes more courage, to apologize publicly when you have been unkind publicly (e.g., when you have said something unkind or unprofessional to an individual or group in a Slack channel).

##### No ego

Don't defend a point to win an argument or double-down on a mistake.
You are not your work; you don't have to defend your point.
You do have to search for the right answer with help from others.

##### Don't let each other fail

Keep an eye out for others who may be struggling or stuck.
If you see someone who needs help, reach out and assist, or connect them with someone else who can provide expertise or assistance.
We succeed and shine together!

##### People are not their work

Always make suggestions about examples of work, not the person.
Say "You didn't respond to my feedback about the design" instead of "You never listen".
And, when receiving feedback, keep in mind that feedback is the best way to improve, and that others giving you feedback want to see you succeed.

##### Do it yourself

Our collaboration value is about helping each other when we have questions, need critique, or need help.
No need to brainstorm, wait for consensus, or
[do with two what you can do yourself](https://www.inc.com/geoffrey-james/collaboration-is-the-enemy-of-innovation.html).
The Bolt Handbook refers to this as the
[Founder Mentality](https://conscious.org/how-a-founders-mentality-encourages-employee-recognition/),
where all team members should approach the problem as if they own the company.

##### Short toes

People joining the company frequently say, "I don't want to step on anyone's toes."
At CoderDojo, we should be more accepting of people taking initiative in trying to improve things.
As organisations grow, their speed of decision-making goes down since there are more people involved.
We should counteract that by having short toes and feeling comfortable letting others contribute to our domain.

##### Collaboration is not consensus

When collaborating, it is always important to stay above radar and work [transparently](#transparency),
but collaboration is [not consensus](/handbook/leadership/#making-decisions) and disagreement is part of collaboration.
You don't need to ask people for their input, and they shouldn't ask you "Why didn't you ask me?"
You don't have to wait for people to provide input, if you did ask them.
You don't need to have everyone agreeing to the same thing - they can [disagree, commit, and disagree](#disagree-commit-and-disagree).
[Two-way doors decisions](#make-two-way-door-decisions) can be reversed as part of
[disagree, commit, and disagree](#disagree-commit-and-disagree), while one-way door decisions benefit from more input.
Recognize these reversible two-way door decisions for when less input is required to iterate faster.
We believe in permissionless innovation—you don't need to involve people, but everyone can contribute.
This is core to how we [iterate](#iteration), since we want smaller teams moving quickly
rather than large teams achieving consensus slowly.

##### Give agency

We give people agency to focus on what they think is most beneficial. If a meeting doesn't seem interesting and
someone's active participation is not critical to the outcome of the meeting, they can always opt to not attend,
or during a video call they can work on other things if they want.
Staying in the call may still make sense even if you are working on other tasks, so other peers can ping you and
get fast answers when needed. This is particularly useful in multi-purpose meetings where you may be involved
for just a few minutes.

##### Write promises down

Agree in writing on measurable goals.

##### Ownership

We expect team members to complete tasks that they are assigned.
You are responsible for executing with attention to detail, connecting the dots across the organization and
anticipating and solving problems. As an owner, you are responsible for overcoming challenges, not suppliers or
other team members. Take initiative and proactively inform stakeholders when there is something you
might not be able to solve.

##### Bias for action

It's important that we keep our focus on action, and don't fall into the trap of analysis paralysis or sticking to a
slow, quiet path without risk. Decisions should be thoughtful, but delivering fast results requires the fearless acceptance
of occasionally making mistakes; our bias for action also allows us to course correct quickly.
Everyone will make mistakes, but it's the relative number of mistakes against all decisions made (i.e. percentage of mistakes),
and the swift correction or resolution of that mistake, which is important. A key to success with transparency is to
always combine an observation with (1) questions (to ensure understanding) *and* (2) suggestions for solutions / improvement
to the group that can take action.  We don't make general complaints without including and supporting the groups that
can effect change. Success with transparency almost always requires effective [collaboration](#collaboration).

##### Disagree, commit, and disagree

Everything can be questioned, but as long as a decision is in place, we expect people to commit to executing it.
Any past decisions and guidelines are open to questioning as long as you act in accordance with them until they are changed.
This is [a common principle](https://ryanestis.com/leadership/disagree-and-commit-to-get-things-done/).
Every decision can be changed;
our [best decision was one that changed an earlier one](https://youtu.be/4BIsON95fl8?t=2034).

In a group setting, participants may disagree with a proposal but not articulate their views for one reason or another.
Sometimes, [many or all individuals may disagree yet choose not to speak up](https://en.wikipedia.org/wiki/Abilene_paradox),
because no one believes they would get agreement from the group. As a result, everyone loses out on their feedback.
[Dissent](https://en.wikipedia.org/wiki/Dissent) is expression of that disagreement.
However, it can be difficult and even socially expensive.
Expression of feedback is a way for everyone to grow and learn, and is
[based on facts rather than opinions](https://hbr.org/tip/2018/04/good-feedback-is-based-on-facts-not-your-opinion).
Share your perspective, rather than agreeing simply to avoid conflict or to go along with everyone else.

When you want to reopen the conversation on something, show that your argument is informed by previous conversations and
[assume the decision was made with the best intent](#assume-positive-intent).
You have to achieve results on every decision while it stands, even when you are trying to have it changed.
You should communicate with the [DRI](/handbook/people-group/directly-responsible-individuals/)
who can change the decision instead of someone who can't.

## ⏱️ Efficiency {#efficiency}

Working efficiently on the right things enables us to make fast progress, which makes our work more fulfilling.
Efficiency is a foundation for [results](#results), because it helps us to achieve more.

##### Write things down

We document everything: in the handbook, in meeting notes, in issues.
We do that because "[the faintest pencil is better than the sharpest memory](https://www.quora.com/What-does-The-faintest-pencil-is-better-than-the-sharpest-memory-mean)."
It is far more efficient to read a document at your convenience than to have to ask and explain.
Having something in version control also lets everyone contribute suggestions to improve it.

##### Boring solutions

Use the simplest and most boring solution for a problem, and remember that [“boring”](http://boringtechnology.club/)
should [not be conflated with “bad” or “technical debt.”](http://mcfunley.com/choose-boring-technology)
The speed of innovation for our organization and product is constrained by the total complexity we have added so far,
so every little reduction in complexity helps.
Don’t pick an interesting technology just to make your work more fun;
using established, popular tech will ensure a more stable and more familiar experience for you and other contributors.

Make a conscious effort to **recognize** the constraints of others within the team.

##### Short verbal answers

Give short answers to verbal questions so the other party has the opportunity to ask more or move on.

##### Keep broadcasts short

Keep one-to-many written communication short, as mentioned in
[this HBR study](https://hbr.org/2016/09/bad-writing-is-destroying-your-companys-productivity):
"A majority say that what they read is frequently ineffective because it’s too long, poorly organized,
unclear, filled with jargon, and imprecise."

##### Accept mistakes

Not every problem should lead to a new process to prevent them. Additional processes make all actions more inefficient;
a mistake only affects one. Once you have accepted the mistake, learn from it. When team members are free to
accept mistakes, they can take more calculated risks.

##### Move fast by shipping the minimal viable change

We value constant improvement by iterating quickly, month after month.
If a task is not the [smallest viable and valuable thing](#iteration), cut the scope.

## 🌐 Diversity, Inclusion & Belonging {#diversity-inclusion}

Diversity, inclusion and belonging are fundamental to the success of CoderDojo. We aim to make a significant impact in our
efforts to foster an environment where everyone can thrive. We are designing a multidimensional approach to ensure that
CoderDojo is a place where people from every background and circumstance feel like they belong and can contribute.

##### Quirkiness

Unexpected and unconventional things make life more interesting.
Celebrate and encourage quirky gifts, habits, behavior, and points of view. Open source is a great way to interact with
interesting people. We try to hire people who think work is a great way to express themselves.

##### Building a safe community

Do **not** make jokes or unfriendly remarks about [characteristics of the people who make up CoderDojo and how they identify](/company/culture/inclusion/#CoderDojos-definition-of-diversity--inclusion).
Everyone has the right to feel safe when working for CoderDojo and/or as a part of the CoderDojo community.
We do not tolerate abuse, [harassment](/handbook/anti-harassment/), exclusion, discrimination, or retaliation by/of any
community members, including our team members.
You can always **refuse** to deal with people who treat you badly and get out of situations that make you feel uncomfortable.

##### Unconscious bias

We recognize that unconscious bias is something that affects everyone and that the
effect it has on us as humans and our organisation is large.
We are responsible for understanding our own implicit biases and helping others
understand theirs. We are continuously [working on getting better at this topic](/company/culture/inclusion/unconscious-bias/).

##### Embracing Neurodiversity

[Neurodiversity](https://www.neurodiversityhub.org/what-is-neurodiversity) refers to variations in the human brain
regarding learning, attention, sociability, mood, and other mental functions. There are various
neurodevelopmental conditions, like autism, ADHD, dyslexia, dyscalculia, dyspraxia, cognitive impairment, schizophrenia,
bipolarity, and other styles of neurodivergent functioning. While neurodivergent individuals often bring
[unique skills and abilities](https://adhdatwork.add.org/potential-benefits-of-having-an-adhd-employee/) which
can be harnessed for a [competitive advantage](https://hbr.org/2017/05/neurodiversity-as-a-competitive-advantage)
in many fields (for example,
[cybersecurity](https://www.forbes.com/sites/samcurry/2019/05/13/neurodiversity-a-competitive-advantage-in-cybersecurity/)),
neurodivergent individuals are often discriminated against. 
Due to non-inclusive hiring practices, they sometimes have trouble making it through traditional hiring processes.
Neurodiversity inclusion best practices benefit everyone, and at CoderDojo, everyone can contribute.

At CoderDojo we embrace Neurodiversity through adopting a variety of different work styles and communication styles,
and we lean into [transparency](#transparency), asynchronous as a default working style, and pre-filled meeting agendas.
These best practices become even more important when embracing neurodiversity. Providing multiple ways to consume
information (written / video / audio) allows everyone to contribute independent of their preferred comprehension style.
It is important to ask team members specifically what their preferred communication method is in order to provide
them information in a format that is easily consumable for them.

Remember, **brains work differently** and always [assume positive intent](#assume-positive-intent), 
even if someone behaves in an unexpected way. While it may be an unexpected behavior to you, it may not be unexpected
to the individual exhibiting the behavior. That is the beauty and value of diversity, 
embracing differences and becoming stronger and better as a result.

## 👣 Iteration {#iteration}

We do the smallest viable and valuable thing, and get it out quickly for feedback. This thing may be additive
(adding something) or subtractive (removing something). If you make suggestions that can be excluded from
the first iteration, turn them into a separate issue that you link. Don't write a large plan; only write the first step.
Trust that you'll know better how to proceed after something is released. You're doing it right if you're slightly
embarrassed by the minimal feature set shipped in the first iteration. This value is the one people most underestimate
when they join CoderDojo. The impact both on your work process and on how much you achieve is greater than anticipated.
In the beginning, it hurts to make decisions fast and to see that things are changed with less consultation.
But frequently, the simplest version turns out to be the best one.

People that join CoderDojo all say they already practice iteration. But this is the value that is the hardest
to understand and adopt. People are trained that if you don't deliver a perfect or polished thing, there will be a
problem. If you do just one piece of something, you have to come back to it. Doing the whole thing seems more efficient,
even though it isn't. If the complete picture is not clear, your work might not be perceived as you want it to be perceived.
It seems better to make a comprehensive product. They see other CoderDojo team members being really effective with iteration
but don't know how to make the transition, and it's hard to shake the fear that constant iteration can lead to shipping
lower-quality work or a worse product. It is possible to ship a minimally viable product while continuing to adhere to
the documented quality standards.

The way to resolve this is to write down only what value you can add with the time you have for this project right now.
That might be 5 minutes or 2 hours. Think of what you can complete in that time that would improve the current situation.
Iteration can be uncomfortable, even painful. If you're doing iteration correctly, it should be. Reverting work back to
a previous state is positive, not negative. We're quickly getting feedback and learning from it. Making a small change
prevented a bigger revert and made it easier to revert.

However, if we take smaller steps and ship smaller, simpler features, we get feedback sooner. Instead of spending
time working on the wrong feature or going in the wrong direction, we can ship the smallest product, receive
fast feedback, and course correct. People might ask why something was not perfect. In that case, mention that it was
an iteration, you spent only "x" amount of time on it, and that the next iteration will contain "y" and be ready on "z".

Iteration enables [results](#results) and [efficiency](#efficiency)

{{< youtube id="2r0UeZ5p57Y" >}}

*In the [CoderDojo Unfiltered video](https://youtu.be/2r0UeZ5p57Y) embedded above, CoderDojo CEO and co-founder
Sid Sijbrandij shares key operating principles to reinforce iteration in an organization.*

##### Set a due date

We always try to set a due date. If needed, we cut scope.
If we have something planned for a specific date, we make that date.
If we planned an announcement for a certain date, we might announce less or indicate what is still uncertain.
But we set a due date because having something out there builds trust and gives us better feedback.

##### Work as part of the community

Small iterations make it easier to work with the wider community. Their work looks more like our work, 
and our work is also quicker to receive feedback.

##### Minimal Viable Change (MVC)

We encourage MVCs to be as small as possible. Always look to make the quickest change possible to improve
the user's outcome. If you validate that the change adds more value than what is there now, then do it.
This may be additive (adding something) or subtractive (removing something). No need to wait for something more robust.
More information is in the [product handbook](/handbook/product/product-principles/#the-minimal-viable-change-mvc),
but this applies to everything we do in all functions. Specifically for product MVCs, there is additional responsibility
to validate with customers that we're adding useful functionality without obvious bugs or usability issues.

##### Everything is in draft

At CoderDojo, we rarely mark any content or proposals as drafts. Everything is always in draft and subject to change.
When everything is in draft, contributions from team members as well as the wider community are welcomed.
By having everything in draft and [assuming others have low context](/company/culture/all-remote/effective-communication/#understanding-low-context-communication),
confusion can be reduced as people have shared access to information.

##### Make two-way door decisions

Most decisions are easy to reverse. In these cases, the [Directly Responsible Individual](/handbook/people-group/directly-responsible-individuals/)
should go ahead and make them without approval. Only when you can't reverse them should there be a more thorough discussion.
By [embracing iteration](#embracing-iteration) and making two-way door decisions, we are more efficient
and achieve more results.

##### Make small merge requests

When you are submitting a merge request for a code change, or a process change in
the handbook, keep it as small as possible. If you are adding a new page to the
handbook, create the new page with a small amount of initial content, get it merged
quickly via [Handbook Usage guidelines](/handbook/handbook-usage/), and then add additional sections iteratively with
subsequent merge requests.
Similarly, when adding features to CoderDojo, consider ways to [reduce the scope](/handbook/product/product-processes/#crafting-an-mvc)
of the feature before creating the merge request to ensure your merge request is as small as possible.

## 👁️ Transparency {#transparency}

Be open about as many things as possible. By making information public, we can reduce the threshold to contribution and make collaboration easier. Use public issue trackers, projects, and repositories when possible. Transparency is not communication. Just because something exists in the handbook or elsewhere doesn't mean it's been communicated to the people who should understand or acknowledge it. On a personal level, be direct when sharing information, and admit when you've made a mistake or were wrong. When something goes wrong, it is a great opportunity to say "What’s the [kaizen](https://en.wikipedia.org/wiki/Kaizen) moment here?" and find a better way without hurt feelings.


##### Public by default

Everything at CoderDojo is public by default.
The public process does two things: allows others to benefit from the conversation and acts as a filter. Since there is only a limited amount of time, we prioritize conversations that a wider audience can benefit from.

One example of transparency at CoderDojo is the [public repository of this website](https://gitlab.com/coderdojo.com/coderdojo-belgium/workgroups/open-source/handbook)
that also contains this [company handbook](/handbook/).
##### Directness

Being direct is about being transparent with each other. We try to channel our inner [Ben Horowitz](https://en.wikipedia.org/wiki/Ben_Horowitz)
by being [both straightforward and kind](https://medium.com/@producthunt/ben-horowitz-s-best-startup-advice-7e8c09c8de1b).
Feedback is always about your work and not your person. That doesn't mean it will be easy to give or receive it.

##### Single Source of Truth

By having most company communications and work artifacts be public to the Internet, we have one single source of truth
for all CoderDojo team members, ninjas, cooks, coaches, and other community members.
We don‘t need separate artifacts with different permissions for different people.

##### Say why, not just what

Transparent changes have the reasons for the change laid out clearly along with the change itself. This leads to fewer questions later on because people already have some understanding. A change with no public explanation can lead to a lot of extra rounds of questioning, which is less efficient.

This also helps with institutional memory: a year from now when you want to know why a decision was made, or not, the issue or MR that has the decision also shares why the decision was made.
This is related to [Chesterton's fence](https://en.wikipedia.org/wiki/Wikipedia:Chesterton%27s_fence) - it's much easier to suggest removing or changing something if you know why it exists in the first place.

Avoid using terms such as "industry standard" or "best practices" as they are vague, opaque, and don't provide enough context as a reason for a change.

Similarly, merely stating a single value isn't a great explanation for why we are making a particular decision. Many things could be considered "iteration" or "efficiency" that don't match our definition of those values. Try to link to an operating principle of the value or provide more context, instead of just saying a single value's name.

Saying why and not just what enables discussion around topics that may impact more than one value; for instance, when weighing the [efficiency of boring solutions](#boring-solutions) with the focus on [customer results](#customer-results). When decisions align with all of our values, they are easy to discuss and decide. When there are multiple values involved, using our [values hierarchy](#hierarchy) and [directly](#directness) discussing the tradeoffs is easier with more context.

Articulating why also helps people understand how something changed when you [articulate that you changed your mind](#articulate-when-you-change-your-mind).

Saying why does not mean justifying a decision against all other suggestions.
The [DRI](/handbook/people-group/directly-responsible-individuals/) is responsible for their decision.
The DRI is not responsible for convincing other people, but they should be able to articulate their reasoning for the change.

When a CoderDojo Team Member comes across an ask or material (MR, handbook, etc.) that does not provide a "why" with sufficient context, the Team Member is responsible for getting the why and, if needed, working with the DRI to ensure that it is adequately documented and communicated to give context to other team members.  In the absence of a why, team members may speculate the why. This is something that can lead to disruption and inefficiency.

## Why have values

Our values provide guidelines on how to behave and are written to be actionable.
They help us describe the type of behavior that we expect from CoderDojo team members.
They help us to know how to behave in the organization and what to expect from others.

Values provide a framework for distributed decision making, detailed in CoderDojo's [TeamOps](/teamops/) management philosophy. They allow individuals to determine what to do without asking their manager and they allow teams to make consistent decisions. When teams across the organization reference the same values in their decision making, there is consistency in how decisions are made. This ensures that [our culture](/company/culture/#culture-at-CoderDojo) remains driven by our values.

Lastly, values create a [conscious culture](https://conscious.org/what-is-conscious-culture-and-why-do-we-need-it/) that is designed to help you prosper and experience exceptional personal growth through work.

## Five dysfunctions

Our values also help us to prevent the [five dysfunctions](https://en.wikipedia.org/wiki/The_Five_Dysfunctions_of_a_Team#Summary):

1. **Fear of conflict** Seeking artificial harmony over constructive passionate debate => *prevented by transparency, specifically* [directness](#directness) *and collaboration, specifically* [short toes](#short-toes)
1. **Absence of trust** Unwilling to be vulnerable within the group => *prevented by collaboration, specifically* [kindness](#kindness)
1. **Avoidance of accountability** Ducking the responsibility to call peers on counterproductive behavior which sets low standards => *prevented by results, iteration, and* [transparency](#transparency)
1. **Inattention to results** Focusing on personal success, status, and ego before team success => *prevented by* [results](#results)
1. **Lack of commitment** Feigning buy-in for group decisions creates ambiguity throughout the organization => *prevented by transparency, specifically* [directness](#directness)

Some dysfunctions are not addressed directly by our values; for example, trust is not one of our values.
Similar to happiness, trust is something that is an outcome, not something you can strive for directly.
We hope that the way we work and our values will instill trust, instead of mandating it from people; trust is earned, not given.

