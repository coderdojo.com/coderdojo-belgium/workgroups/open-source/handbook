module gitlab.com/coderdojo.com/coderdojo-belgium/workgroups/open-source/handbook

go 1.19

require (
	github.com/google/docsy v0.7.1 // indirect
	github.com/google/docsy/dependencies v0.7.1 // indirect
	github.com/hairyhenderson/go-codeowners v0.4.0 // indirect
	gitlab.com/gitlab-com/content-sites/docsy-gitlab v0.2.9 // indirect
)
